package com.nCinga.bo;

import java.util.Objects;

public class Product {

    private final int productId;
    private final int sellerId;
    private final String productName;
    private final int productPrice;
    private int availableQuantity;

    public Product(int productId, int sellerId, String productName, int productPrice, int availableQuantity) {
        this.productId = productId;
        this.sellerId = sellerId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.availableQuantity = availableQuantity;
    }

    public int getProductId() {
        return productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getProductName() {
        return productName;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public String toString() {
        return "Product{productId=" + productId +
                ", sellerId=" + sellerId +
                ", productName=" + productName +
                ", productPrice=" + productPrice +
                ", availableQuantity=" + availableQuantity +
                "}";
    }

    public void decreaseAvailableCount(int buyCount){
        this.availableQuantity = this.availableQuantity - buyCount;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId == product.productId && sellerId == product.sellerId && productPrice == product.productPrice &&
                availableQuantity == product.availableQuantity && productName.equals(product.productName);
    }

    public int hashCode() {
        return Objects.hash(productId, sellerId, productName, productPrice, availableQuantity);
    }
}
