package com.nCinga.bo;

import java.util.Objects;

public class Buyer {

    private int buyerId;
    private static int autoIncId = 0;
    private final String buyerName;

    public Buyer(String buyerName) {
        this.buyerId = autoIncrementId();
        this.buyerName = buyerName;
    }

    private int autoIncrementId() {
        return ++autoIncId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public String toString() {
        return "Buyer{buyerId=" + buyerId + ", buyerName=" + buyerName + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Buyer buyer = (Buyer) o;
        return buyerId == buyer.buyerId && buyerName.equals(buyer.buyerName);
    }

    public int hashCode() {
        return Objects.hash(buyerId, buyerName);
    }
}
