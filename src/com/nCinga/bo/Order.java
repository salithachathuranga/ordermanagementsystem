package com.nCinga.bo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Order {

    private int oderId;
    private static int autoIncId = 0;
    private int buyerId;
    private final int productId;
    private final int orderedQuantity;
    private final String orderedDate;

    public Order(int buyerId, int productId, int orderedQuantity) {
        this.oderId = autoIncrementId();
        this.buyerId = buyerId;
        this.productId = productId;
        this.orderedQuantity = orderedQuantity;
        this.orderedDate = setOrderedDate();
    }

    private String setOrderedDate(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    private int autoIncrementId() {
        return ++autoIncId;
    }

    public int getOderId() {
        return oderId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public int getProductId() {
        return productId;
    }

    public int getOrderedQuantity() {
        return orderedQuantity;
    }

    public String getOrderedDate() {
        return orderedDate;
    }

    public String toString() {
        return "Order{oderId=" + oderId + ", buyerId=" + buyerId + ", productId=" + productId +
                ", orderedQuantity=" + orderedQuantity + ", orderedDate=" + orderedDate +
                "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return oderId == order.oderId && buyerId == order.buyerId && productId == order.productId &&
                orderedQuantity == order.orderedQuantity && orderedDate.equals(order.orderedDate);
    }

    public int hashCode() {
        return Objects.hash(oderId, buyerId, productId, orderedQuantity, orderedDate);
    }
}
