package com.nCinga.bo;

import java.util.Objects;

public class Seller {

    private final int sellerId;
    private final String sellerName;

    public Seller(int sellerId, String sellerName) {
        this.sellerId = sellerId;
        this.sellerName = sellerName;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String toString() {
        return "Seller{sellerId=" + sellerId + ", sellerName=" + sellerName + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seller seller = (Seller) o;
        return sellerId == seller.sellerId && sellerName.equals(seller.sellerName);
    }

    public int hashCode() {
        return Objects.hash(sellerId, sellerName);
    }
}
