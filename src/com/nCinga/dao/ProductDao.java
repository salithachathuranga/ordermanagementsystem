package com.nCinga.dao;

import com.nCinga.bo.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDao {

    private List<Product> productList;
    private static ProductDao productDao;

    private ProductDao() {
        this.productList = new ArrayList<>();
        initStoreItems();
    }

    public static ProductDao getInstance() {
        productDao = productDao == null ? new ProductDao() : productDao;
        return productDao;
    }

    public List<Product> getProducts() {
        return productList;
    }

    private void initStoreItems() {
        productList.add(new Product(1, 1, "Maggie Noodles", 70, 10));
        productList.add(new Product(2, 1, "Jack Mackerel", 100, 50));
        productList.add(new Product(3, 2, "Lux Soap", 60, 80));
        productList.add(new Product(4, 3, "Mango Nectar", 120, 60));
        productList.add(new Product(5, 4, "Garlic Bites", 40, 70));
    }

    public List<Product> getProductsForSeller(int sellerId) {
        List<Product> productsOfSeller = new ArrayList<>();
        for (Product product : productList) {
            if (product.getSellerId() == sellerId) {
                productsOfSeller.add(product);
            }
        }
        return productsOfSeller;
    }

    public Product getProductDetailsById(int productId) {
        for (Product product : productList) {
            if (product.getProductId() == productId) {
                return product;
            }
        }
        return null;
    }

    public boolean isProductExists(int productId) {
        for (Product product : productList) {
            if(product.getProductId() == productId){
                return true;
            }
        }
        return false;
    }

    public boolean isQuantityAvailable(int productId, int quantity){
        for (Product product : productList) {
            if(product.getProductId() == productId){
                if(product.getAvailableQuantity() > 0 && product.getAvailableQuantity() >= quantity){
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        return false;
    }

    public void addProductToStore(Product product){
        productList.add(product);
    }

    public void removeProductFromStore(Product product){
        productList.remove(product);
    }

}
