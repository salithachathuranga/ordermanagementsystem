package com.nCinga.dao;

import com.nCinga.bo.Seller;

import java.util.ArrayList;
import java.util.List;

public class SellerDao {

    private List<Seller> sellerList;
    private static SellerDao sellerDao;

    private SellerDao() {
        this.sellerList = new ArrayList<>();
        sellerList.add(new Seller(1,"Nathan James"));
        sellerList.add(new Seller(2,"Robin Peterson"));
        sellerList.add(new Seller(3,"Michale Clarke"));
        sellerList.add(new Seller(4,"David Jones"));
        sellerList.add(new Seller(5,"Mike Pollard"));
    }

    public static SellerDao getInstance(){
        sellerDao = sellerDao == null ? new SellerDao() : sellerDao;
        return sellerDao;
    }

    public List<Seller> getSellerList() {
        return sellerList;
    }

    public Seller getSellerDetailsById(int sellerId) {
        for (Seller seller : sellerList) {
            if (seller.getSellerId() == sellerId) {
                return seller;
            }
        }
        return null;
    }

    public boolean isSellerExists(int sellerId){
        Seller seller = getSellerDetailsById(sellerId);
        if(sellerList.contains(seller)){
            return true;
        }
        return false;
    }

    public void addSeller(Seller seller){
        sellerList.add(seller);
    }

    public void removeSeller(Seller seller){
        sellerList.remove(seller);
    }

}
