package com.nCinga.dao;

import com.nCinga.bo.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderDao {

    private List<Order> orderList;
    private static OrderDao orderDao;

    private OrderDao(){
        this.orderList = new ArrayList<>();
        orderList.add(new Order(1,2,2));
        orderList.add(new Order(2,2,5));
        orderList.add(new Order(3,3,3 ));
        orderList.add(new Order(1,1,1));
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public Order getOrderDetailsById(int orderId){
        for (Order order : orderList){
            if(order.getOderId() == orderId){
                return order;
            }
        }
        return null;
    }

    public List<Order> getOrdersForBuyer(int buyerId){
        List<Order> orders = new ArrayList<>();
        for (Order order : orderList){
            if(order.getBuyerId() == buyerId){
                orders.add(order);
            }
        }
        return orders;
    }

    public void addOrderToList(Order order){
        orderList.add(order);
    }

    public void removeOrderFromList(Order order){
        orderList.remove(order);
    }
}
