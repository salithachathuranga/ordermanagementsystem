package com.nCinga.dao;

import com.nCinga.bo.Buyer;

import java.util.ArrayList;
import java.util.List;

public class BuyerDao {

    private List<Buyer> buyerList;
    private static BuyerDao buyerDao;

    private BuyerDao() {
        this.buyerList = new ArrayList<>();
        buyerList.add(new Buyer( "salitha"));
    }

    public static BuyerDao getInstance() {
        buyerDao = buyerDao == null ? new BuyerDao() : buyerDao;
        return buyerDao;
    }

    public List<Buyer> getBuyerList() {
        return buyerList;
    }

    public Buyer getBuyerDetailsById(int buyerId) {
        for (Buyer buyer : buyerList) {
            if (buyer.getBuyerId() == buyerId) {
                return buyer;
            }
        }
        return null;
    }

    public boolean isBuyerExists(int buyerId){
        Buyer buyer = getBuyerDetailsById(buyerId);
        if(buyerList.contains(buyer)){
            return true;
        }
        return false;
    }

    public void addBuyer(Buyer buyer){
        buyerList.add(buyer);
    }

    public void removeBuyer(Buyer buyer){
        buyerList.remove(buyer);
    }
}
