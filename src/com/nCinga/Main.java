package com.nCinga;

import com.nCinga.service.OrderService;
import com.nCinga.service.ProductService;
import com.nCinga.service.impl.OrderServiceImpl;
import com.nCinga.service.impl.ProductServiceImpl;
import java.util.Scanner;

public class Main {

    static void greeting(){
        System.out.println("====================================================");
        System.out.println("WELCOME TO SUPER STORE");
        System.out.println("====================================================");
        System.out.println("What are going to do in our store?");
        System.out.println("1. View the products in store");
        System.out.println("2. Order a product");
        System.out.println("3. View your orders");
        System.out.println("4. View orders for a particular seller");
        System.out.println("5. View products for a particular seller");
        System.out.println("====================================================");
        System.out.print("Select any operation you need to perform: ");
    }

    public static void main(String[] args) throws Exception {

        ProductService productService = new ProductServiceImpl();
        OrderService orderService = new OrderServiceImpl();

        greeting();
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        System.out.println("You have selected option: "+option);
        System.out.println("====================================================");
        switch (option){
            case 1:
                System.out.println("\nVIEW THE PRODUCTS IN STORE");
                System.out.println();
                productService.viewProducts();
                break;
            case 2:
                System.out.println("\nBUY A PRODUCT FROM STORE");
                System.out.print("\nGive Product ID: ");
                int prodId = scanner.nextInt();
                System.out.print("Quantity you need: ");
                int quantity = scanner.nextInt();
                orderService.placeOrder(prodId,quantity);
                break;
            case 3:
                System.out.println("\nVIEW YOUR ORDERS");
                System.out.print("\nGive your ID: ");
                int buyerId = scanner.nextInt();
                System.out.println();
                orderService.viewOrdersForBuyer(buyerId);
                break;
            case 4:
                System.out.println("\nVIEW THE ORDERS DONE BY SELLER");
                System.out.print("\nGive the ID of the Seller: ");
                int sellerId = scanner.nextInt();
                System.out.println();
                orderService.viewOrdersForSeller(sellerId);
                break;
            case 5:
                System.out.println("\nVIEW THE PRODUCTS OWNED BY SELLER");
                System.out.print("\nGive the ID of the Seller: ");
                int sellerId2 = scanner.nextInt();
                System.out.println();
                productService.viewProductsForSeller(sellerId2);
                break;
            default:
                System.out.println("INVALID OPERATION...BYE!");
                break;
        }
        scanner.close();

    }
}
