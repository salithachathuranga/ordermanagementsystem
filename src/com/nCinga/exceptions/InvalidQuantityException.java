package com.nCinga.exceptions;

public class InvalidQuantityException extends RuntimeException {

    public InvalidQuantityException(String msg) {
        super(msg);
    }
}
