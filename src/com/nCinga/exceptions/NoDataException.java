package com.nCinga.exceptions;

public class NoDataException extends RuntimeException {

    public NoDataException(String msg) {
        super(msg);
    }

}
