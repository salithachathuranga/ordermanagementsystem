package com.nCinga.service.impl;

import com.nCinga.bo.Seller;
import com.nCinga.dao.SellerDao;
import com.nCinga.exceptions.NoDataException;
import com.nCinga.exceptions.NotFoundException;
import com.nCinga.service.SellerService;

public class SellerServiceImpl implements SellerService {

    private SellerDao sellerDao;

    public SellerServiceImpl(){
        sellerDao = SellerDao.getInstance();
    }

    public Seller viewSellerById(int sellerId) {
        if (sellerDao.getSellerList().size() > 0){
            if (sellerDao.isSellerExists(sellerId)){
                return sellerDao.getSellerDetailsById(sellerId);
            }
            else {
                throw new NotFoundException("No Seller with ID: "+sellerId);
            }
        }
        throw new NoDataException("No Sellers Found!");
    }
}
