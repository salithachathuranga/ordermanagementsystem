package com.nCinga.service.impl;

import com.nCinga.bo.Product;
import com.nCinga.dao.ProductDao;
import com.nCinga.exceptions.NoDataException;
import com.nCinga.exceptions.NotFoundException;
import com.nCinga.service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    public ProductServiceImpl() {
        productDao = ProductDao.getInstance();
    }

    public void viewProducts() {
        List<Product> products = productDao.getProducts();
        if (products.size() > 0) {
            for (Product product : products) {
                System.out.println("Product ID: " + product.getProductId());
                System.out.println("Seller ID: " + product.getSellerId());
                System.out.println("Product Name: " + product.getProductName());
                System.out.println("Unit Price: " + product.getProductPrice());
                System.out.println("Available Quantity: " + product.getAvailableQuantity());
                System.out.println();
            }
        }
        else {
            throw new NoDataException("No Products in Store!");
        }
    }

    public void viewProductsForSeller(int sellerId){
        List<Product> productsOfSeller = productDao.getProductsForSeller(sellerId);
        if (productsOfSeller.size() > 0){
            for (Product product : productsOfSeller) {
                System.out.println("Product ID: " + product.getProductId());
                System.out.println("Seller ID: " + product.getSellerId());
                System.out.println("Product Name: " + product.getProductName());
                System.out.println("Unit Price: " + product.getProductPrice());
                System.out.println("Product Quantity: " + product.getAvailableQuantity());
                System.out.println();
            }
        }
        else {
            throw new NotFoundException("No Products for Seller ID: "+sellerId);
        }
    }

    public void viewProductById(int productId){
        Product product = productDao.getProductDetailsById(productId);
        if (product != null){
            System.out.println("Product ID: " + product.getProductId());
            System.out.println("Seller ID: " + product.getSellerId());
            System.out.println("Product Name: " + product.getProductName());
            System.out.println("Unit Price: " + product.getProductPrice());
            System.out.println("Product Quantity: " + product.getAvailableQuantity());
            System.out.println();
        }
        else {
            throw new NotFoundException("Product is not found in store!");
        }
    }
}
