package com.nCinga.service.impl;

import com.nCinga.bo.Buyer;
import com.nCinga.dao.BuyerDao;
import com.nCinga.exceptions.NoDataException;
import com.nCinga.exceptions.NotFoundException;
import com.nCinga.service.BuyerService;

public class BuyerServiceImpl implements BuyerService {

    private BuyerDao buyerDao;

    public BuyerServiceImpl(){
        buyerDao = BuyerDao.getInstance();
    }

    public Buyer viewBuyerById(int buyerId) {
        if (buyerDao.getBuyerList().size() > 0){
            if (buyerDao.isBuyerExists(buyerId)){
                return buyerDao.getBuyerDetailsById(buyerId);
            }
            else {
                throw new NotFoundException("No Buyer with ID: "+buyerId);
            }
        }
        throw new NoDataException("No Buyers Found!");
    }
}
