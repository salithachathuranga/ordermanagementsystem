package com.nCinga.service.impl;

import com.nCinga.bo.Buyer;
import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.dao.BuyerDao;
import com.nCinga.dao.OrderDao;
import com.nCinga.dao.ProductDao;
import com.nCinga.dao.SellerDao;
import com.nCinga.exceptions.InvalidQuantityException;
import com.nCinga.exceptions.NoDataException;
import com.nCinga.exceptions.NotFoundException;
import com.nCinga.service.OrderService;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;
    private ProductDao productDao;
    private SellerDao sellerDao;
    private BuyerDao buyerDao;

    public OrderServiceImpl() {
        orderDao = OrderDao.getInstance();
        productDao = ProductDao.getInstance();
        sellerDao = SellerDao.getInstance();
        buyerDao = BuyerDao.getInstance();
    }

    public void viewOrderById(int orderId) {
        Order orderData = orderDao.getOrderDetailsById(orderId);
        if (orderData != null) {
            System.out.println("Order ID: " + orderData.getOderId());
            System.out.println("Buyer ID: " + orderData.getBuyerId());
            System.out.println("Product ID: " + orderData.getProductId());
            System.out.println("Order Quantity: " + orderData.getOrderedQuantity());
            System.out.println("Order Date: " + orderData.getOrderedDate());
        } else {
            throw new NotFoundException("No Order with ID: " + orderId);
        }

    }

    public void viewOrdersForBuyer(int buyerId) {
        if (buyerDao.isBuyerExists(buyerId)){
            List<Order> orderList = orderDao.getOrdersForBuyer(buyerId);
            if (orderList.size() > 0) {
                for (Order order : orderList) {
                    System.out.println("Order ID: " + order.getOderId());
                    System.out.println("Buyer ID: " + order.getBuyerId());
                    System.out.println("Product ID: " + order.getProductId());
                    System.out.println("Order Quantity: " + order.getOrderedQuantity());
                    System.out.println("Order Date: " + order.getOrderedDate());
                    System.out.println();
                }
            }
            else {
                throw new NoDataException("No Orders For this Buyer!");
            }
        }
        else {
            throw new NotFoundException("No Buyer with ID: "+buyerId);
        }
    }

    private List<Order> getOrdersForSeller(int sellerId){
        List<Order> ordersForSeller = new ArrayList<>();
        if (sellerDao.isSellerExists(sellerId)){
            List<Product> productList = ProductDao.getInstance().getProductsForSeller(sellerId);
            for (Order order : orderDao.getOrderList()) {
                for (Product product : productList) {
                    if (order.getProductId() == product.getProductId()) {
                        ordersForSeller.add(order);
                    }
                }
            }
        }
        else {
            throw new NotFoundException("No Seller with ID: "+sellerId);
        }
        return ordersForSeller;
    }

    public void viewOrdersForSeller(int sellerId) {
        List<Order> orderList = getOrdersForSeller(sellerId);
        if (sellerDao.isSellerExists(sellerId)){
            if (orderList.size() > 0) {
                for (Order order: orderList) {
                    System.out.println("Order ID: " + order.getOderId());
                    System.out.println("Buyer ID: " + order.getBuyerId());
                    System.out.println("Product ID: " + order.getProductId());
                    System.out.println("Order Quantity: " + order.getOrderedQuantity());
                    System.out.println("Order Date: " + order.getOrderedDate());
                    System.out.println();
                }
            }
            else {
                throw new NoDataException("No orders for Seller ID: "+sellerId);
            }
        }
        else{
            throw new NotFoundException("No Seller with ID: "+sellerId);
        }

    }

    public void placeOrder(int productId, int quantity) {
        if(productDao.isProductExists(productId)){
            Product product = productDao.getProductDetailsById(productId);
            if(productDao.isQuantityAvailable(productId,quantity)){
                System.out.print("Give your name: ");
                Scanner scanner = new Scanner(System.in);
                String buyerName = scanner.nextLine();
                Buyer buyer = new Buyer(buyerName);
                Order newOrder = new Order(buyer.getBuyerId(), productId, quantity);
                product.decreaseAvailableCount(quantity);
                orderDao.addOrderToList(newOrder);
                System.out.println("====================================================");
                System.out.println();
                System.out.println("Order Receipt: ");
                System.out.println("Order Id: "+newOrder.getOderId());
                System.out.println("Customer: "+buyerName);
                System.out.println("Product Name: "+product.getProductName());
                System.out.println("Unit Price: "+product.getProductPrice());
                System.out.println("Quantity: "+newOrder.getOrderedQuantity());
                System.out.println("Total Price: "+product.getProductPrice()*quantity);
                System.out.println();
                System.out.println("====================================================");
                System.out.println("Thank You for coming!");
            }
            else {
                throw new InvalidQuantityException("Store has not enough stock for the product!");
            }
        }
        else {
            throw new NotFoundException("Product is not found in store!");
        }
    }
}
