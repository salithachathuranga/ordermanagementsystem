package com.nCinga.service;

import com.nCinga.bo.Buyer;
import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.bo.Seller;

import java.util.List;

public interface OrderService {

    void viewOrderById(int orderId);

    void viewOrdersForBuyer(int buyerId);

    void viewOrdersForSeller(int sellerId);

    void placeOrder(int productId, int quantity);
}
