package com.nCinga.service;

import com.nCinga.bo.Buyer;

public interface BuyerService {

    Buyer viewBuyerById(int buyerId);
}
