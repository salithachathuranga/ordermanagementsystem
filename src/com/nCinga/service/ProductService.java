package com.nCinga.service;

import com.nCinga.bo.Product;

import java.util.List;

public interface ProductService {

    void viewProducts();

    void viewProductById(int productId);

    void viewProductsForSeller(int sellerId);
}
