package com.nCinga.service;


import com.nCinga.bo.Seller;

public interface SellerService {

    Seller viewSellerById(int sellerId);
}
